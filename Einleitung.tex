\section{Einleitung}
\label{sec:einleitung}
Die vorliegende Arbeit behandelt die Dokumentation des Prozesses von der Problem- und Anforderungsanalyse bis zur prototypischen Umsetzung einer Online-Plattform. Diese Online-Plattform dient der interaktiven, multimedialen Dokumentation und Kommentierung von partizipativ erstellten
Szenariomodellen.

\subsection{Themeneinführung}
\label{subsec:themeneinführung}
Das dieser Arbeit übergeordnete Forschungsprojekt \textit{regionale Diskurse zur Bioökonomie} (DiReBio) hat zum Ziel, verschiedene Stakeholder einer Region zusammenzubringen und gemeinsam Pläne für eine bioökonomische Transformation eben dieser Region erarbeiten und diskutieren zu lassen.

Unter einer bioökonomischen Transformation wird die Umwandlung der herkömmlichen Wirtschaft in eine Ökonomie, in welcher die Grundbausteine für Materialien, Chemikalien, Energie etc. komplett aus erneuerbaren Energien stammen, verstanden \cite{McCormick_2013}. Es handelt sich somit um einen nachhaltigen, auf regenerativen Werkstoffen und Energiequellen basierenden und regionalen Wirtschaftskreislauf. Der Gedankenaustausch zu dieser Transformation geschieht über lokal durchgeführte Workshops, in denen Szenarien erarbeitet und in physischen Modellen dargestellt werden. Im Anschluss an diese Workshops sollen die Ergebnisse durch die Öffentlichkeit begutachtet werden. Die Öffentlichkeit hat die Möglichkeit, konstruktive Beiträge zu einem erarbeiteten Modell einzubringen \cite{direbio_projekt}. Die Erarbeitung und prototypische Entwicklung der Möglichkeit des Austauschs ist Gegenstand dieser Bachelorarbeit.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.75\linewidth]{ueberblick_ergebnis_workshop.jpg}
	\end{center}
	\caption{Endergebnis einer Szenariomodellierung}
	\label{fig:workshop_result_overview}
\end{figure}

In einem Workshop wird in verschiedenen Stufen ein Szenario entwickelt und mittels einer Karte physisch modelliert. Da die hier behandelte Online-Plattform einzig das Endergebnis des Workshops betrachtet, sind die Details der einzelnen Entwicklungsstufen dessen für diese Bachelorarbeit unerheblich und werden deswegen nicht weiter betrachtet.

Das Ergebnis eines solchen Workshops ist ein der realen Umgebung nachempfundenes, aber nicht unbedingt korrektes oder maßstabgetreues, haptisches Landschaftsmodell (s. Abbildung \ref{fig:workshop_result_overview}). In dem dargestellten Modell ist eine fertig modellierte Landschaft einer nicht näher genannten Region zu sehen.
Diese Landschaft wird später die Basis für die Beiträge auf der Online-Plattform sein.

\subsection{Aufgabenstellung}
\label{subsec:aufgabenstellung}
Die gegebene Aufgabenstellung umfasste die Konzeption und prototypische Erstellung einer Online-Plattform zum Einsammeln von Feedback aus der Bevölkerung und von relevanten Stakeholdern zu einem durchgeführten Workshop des Projekts \textit{DiReBio}. Um das zu erreichen, galt es zunächst herauszufinden, ob bereits entsprechende Plattformen existieren, die verwendet werden können oder ob eine eigene Plattform entwickelt werden muss.

\subsection{Methodik}
\label{subsec:methodik}
Als Vorgehensmodell wurde \textit{design science research process} (DSRP) nach Peffers et al. \cite{dsrp} gewählt (siehe Abbildung \ref{fig:peffers}).

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=\linewidth]{Peffers.pdf}
	\end{center}
	\caption[Eigene Darstellung des in dieser Arbeit durchlaufenden Ausschnitts aus dem DSRP nach Peffers et al.]{Eigene Darstellung des in dieser Arbeit durchlaufenden Ausschnitts aus dem DSRP nach Peffers et al. \cite{dsrp}}
	\label{fig:peffers}
\end{figure}

DSRP fokussiert sich, wie für Design Science Research üblich, auf die Erstellung von Artefakten mit der Absicht, die Artefakte zu verbessern. Typischerweise wird Design Science Research u.A. auf die Erstellung von Human-Computer Interfaces (HCI) angewandt. Obwohl sie nicht darauf beschränkt ist, wird sie allgemein meistens im Feld der Informatik verwendet \cite{vaishnavi_2008}. Da sich diese Bachelorarbeit mit einem der Informatik nahen Problem befasst und am Ende eine gewisse Art Human-Computer Interface entsteht, eignet sich der DSRP gut für diese Arbeit und wurde deswegen als Vorgehensmodell zur Erstellung dieser Bachelorarbeit ausgewählt.

Ein weiterer Vorteil dieses Vorgehensmodells ist, dass es dem in der Softwareentwicklung etablierten Wasserfallmodell sehr ähnelt, was es erlaubt, das DSRP sowohl für den Softwareentwicklungsteil, als auch den Forschungsteil dieser Arbeit zu verwenden. 

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=\linewidth]{Wasserfall.pdf}
	\end{center}
	\caption{Eigene Darstellung eines Wasserfallmodells}
	\label{fig:wasserfall}
\end{figure}

Beim Wasserfall (siehe Abbildung \ref{fig:wasserfall}) ist die Wartung ausgeklammert, da sie in dieser Arbeit aufgrund der Beschaffenheit einer Bachelorarbeit nicht vorkommen kann. Durch die zeitliche Limitierung einer Bachelorarbeit muss auf die Wartung verzichtet werden. Doch auch in Softwareentwicklungsprozessen der freien Wirtschaft ist die Wartung nicht als selbstverständlich gegeben, da sie ein eigener Verhandlungsgegenstand ist. Der Auftraggeber kann eigenmächtig entscheiden, ob er eine Wartung in Auftrag gibt.

Ein Ziel des DSRP is die Erstellung eines Artefakts. Im Fall dieser Bachelorarbeit ist das Artefakt die Software, welche entwickelt wurde. Das DSRP besteht gemäß Peffers et al. aus sechs Stufen, die in Abbildung \ref{fig:peffers} vereinfacht dargestellt wurden \cite{dsrp}:
\begin{enumerate}
	\item Problem identification and motivation\\
	Hier wird das Forschungsproblem bzw. die Forschungslücke definiert und der zu erreichende Zielwert bestimmt. Da diese Definition genutzt wird, um später das Artefakt zu entwickeln, sollte das Problem hier möglichst genau beschrieben und im Falle eines komplexen Problems möglichst feingranular aufgeschlüsselt werden.
	\item Objectives of a solution\\
	Die Ziele werden aus den Problemen abgeleitet. Hier werden auch die Kriterien aufgeführt, welche bestimmen, ob ein Artefakt vorteilhafter ist als bereits bestehende Lösungen.
	\item Design and development\\
	In diesem Schritt wird das zu erstellende Artefakt konzipiert und erstellt. Es werden dabei alle gewünschten Funktionalitäten definiert und implementiert. Hier ist außerdem auf das Wissen einzugehen, welches gebraucht wird, um das Artefakt zu erzeugen.
	\item Demonstration\\
	Hier wird gezeigt, dass das erzeugte Artefakt tatsächlich die unter Schritt 1 definierten Probleme löst. Dies kann durch Experimente, Simulationen, Fallstudien, Beweise oder einem beliebigen anderen geeigneten Verfahren erreicht werden.
	\item Evaluation\\
	In diesem Schritt werden die Resultate aus der Demonstration genommen, um anhand geeigneter Metriken zu überprüfen, ob die Ziele aus Schritt 2 tatsächlich erreicht werden. Geeignete Metriken könnten z.B. ein einfacher Vergleich zwischen gesetzten und erreichten Zielen, Zufriedenheitsumfragen, Feedback der Benutzer:innen oder Simulationen sein. Sollten die Metriken nicht zufriedenstellend erfüllt werden, kann von hier zurück zu Schritt 3 iteriert werden.
	\item Communication\\
	Teil der Kommunikation ist es mitzuteilen, warum das hervorgebrachte Artefakt neu und einzigartig ist und warum es der Zielgruppe effektiv hilft. 
\end{enumerate}

Der Einstieg dieser Arbeit geschieht dabei über den \textit{Problemzentrierten Ansatz} (siehe Abbildung \ref{fig:peffers}) direkt bei Schritt 1 \textit{Problem identification and motivation}. Dabei wird das bestehende Problem analysiert, um die Problemdefinition zu nutzen, ein Artefakt zu erzeugen, welches dieses Problem löst.	

\subsection{Aufbau der Arbeit}
\label{subsec:aufbau_der_arbeit}
Dem DSRP nach Peffers entsprechend wird diese Arbeit mit einem Kapitel über die Problemanalyse beginnen. In der Problemanalyse werden grundlegende Begriffe geklärt und es wird analysiert, wie der zu Grunde liegende Workshop aufgebaut ist und wie dieser dokumentiert und kommentiert werden kann. Außerdem wird auf die technische Kompetenz der zu erwartenden Endnutzer geschaut.

Im nachfolgenden Kapitel, der Zielanalyse, wird auf den Entwicklungsprozess, die Anforderungen und den Zielentwurf eingegangen. Das Ziel dieses Kapitels ist, der Konzeption und Entwicklung den Weg zu bereiten und das angestrebte Ziel zu definieren.

Im vierten Kapitel, der Konzeption und Entwicklung, wird darauf eingegangen, warum bereits bestehende Softwarelösungen für das Projekt \textit{DiReBio} nicht in Frage kamen. Weiterhin werden die Ziele aus der Zielanalyse in Anforderungen formuliert und die optimale Benutzer:innenführung, sowie der allgemeine Aufbau besprochen. Hier wird das nach Peffers entstehende Artefakt besprochen.

Im Anschluss folgt die Demonstration und Evaluation nach Peffers, in welcher Überprüft wird, inwiefern die gesteckten Ziele erreicht wurden. Als letzter Schritt nach Peffers, der Kommunikation, ist das hier vorliegende Dokument zu verstehen.