\section{Zielanalyse}
\label{sec:zielanalyse}
Im nachfolgenden Kapitel wird dargelegt, wie die im vorherigen Kapitel genannten groben Anforderungen weiterentwickelt und verfeinert wurden, um letzten Endes zu umsetzbaren und überprüfbaren Anforderungen formuliert zu werden. Um die anfangs möglichst unklaren Anforderungen des Kunden zu klären und flexibel auf Änderungen zurückgreifen zu können, wurde auf die Mittel des Requirements Engineerings zurückgegriffen.

\subsection{Grober Zielentwurf}
\label{subsec:grober_zielentwurf}
Wie in der \nameref{sec:problemanalyse} besprochen, muss der Wissensstand derjenigen, die Feedback hinterlassen wollen, auf einen einheitlichen Stand gebracht werden, um die Qualität des gegebenen Feedbacks hoch zu halten. Gemeinsam mit den anderen in \textit{\nameref{subsec:gegebenheiten_und_probleme_durch_den_aufbau_des_workshops}} (s. Kapitel~\ref{subsec:gegebenheiten_und_probleme_durch_den_aufbau_des_workshops}) besprochenen Anforderungen ergibt sich aus folgenden Gründen das Ziel, eine Online-Plattform zu entwickeln. Eine Online-Plattform kann zu jeder Zeit, von jedem Ort, unter Zuhilfenahme eines geeigneten Endgeräts erreicht werden. Ausgehend davon, dass geschultes Personal, welches in die Region fährt, um von Einzelpersonen oder Stakeholdern Feedback einzusammeln, nicht existent ist, muss eine Möglichkeit geschaffen werden, die auch ohne dieses Personal auskommt.

Gemäß des Digitalindexes 2018/2019 haben 64~\% der Deutschen Zugriff auf ein Notebook, 46~\% auf einen Desktop-PC und 75~\% auf ein Smartphone \cite{d21_2019}. Entsprechend ist anzunehmen, dass ein Großteil der Menschen ein Endgerät besitzt, welches den Zugriff auf eine zur Verfügung gestellte Plattform ermöglicht. Eine Online-Plattform wäre rund um die Uhr verfügbar. Sie könnte diverses Material zur Verfügung stellen, um die Menschen zu bilden. Des Weiteren könnte sie verschiedene Angebote beinhalten, um bei Eingabe- und Verständnisschwierigkeiten auszuhelfen. Außerdem wäre eine Online-Plattform in einem zeitlich vertretbaren Aufwand von Feedback gebenden Personen zu benutzen. Mit dieser Plattform könnte das Wissen von Stakeholdern und anderen interessierten Personen einer Region zusammengeführt werden. Ohne eine solche Plattform wäre dies nur schwer oder mit einem großen Personalaufwand möglich. Sie würde entsprechend alle aufgestellten Anforderungen (s. Kapitel \ref{subsec:anforderungen}) abdecken. 

Folglich ist das Ziel eine Plattform, welche die Menschen zuerst bildet und am Ende einen qualifizierten Beitrag von der Person abfragt. Wie die genauen Features ausgearbeitet wurden, folgt im nachfolgenden Unterkapitel.

\subsection{Herangehensweise im Entwicklungsprozess}
\label{subsec:herangehensweise_im_entwicklungsprozess}

Da die genauen Anforderungen an die Plattform zu Beginn der Arbeit noch sehr ungenau waren, musste ein Verfahren genutzt werden, mit welcher die genauen Anforderungen des Kunden ermittelt werden können. Als Verfahren wurde hierfür das \textit{Requirements Engineering} verwendet. Im Folgenden wird zuerst geklärt, was Requirements Engineering bedeutet.

Die Prozesse, welche zum Requirements Engineering genutzt werden, variieren abhängig von der Anwendungsdomäne der involvierten Menschen und der Organisation, welche die Anforderungen erarbeitet. Der Kern, der in jedem Prozess gleich ist, umfasst die folgenden Punkte:
\begin{itemize}
	\item Anforderungserhebung
	\item Anforderungsanalyse
	\item Anforderungsvalidierung
	\item Anforderungsmanagement
\end{itemize}
Dabei beschreibt das Anforderungsmanagement den Prozess des Schreibens und aktuell Haltens entsprechender Dokumente wie z.B. Spezifikationen und das Einpflegen neuer Anforderungen sowie die Planung, Organisation und Kontrolle des Requirements Engineering \cite{sommerville_2011}.

Eine Anforderung ist eine Eigenschaft, die eine Software besitzen muss sowie Rahmenbedingungen für den Lebenszyklus (Entwicklung, Betrieb, Wartung), welche beachtet werden müssen \cite{ieee_610.12-1990}. Anforderungen werden des Weiteren in funktionale Anforderungen und nicht funktionale Anforderungen unterteilt. Funktionale Anforderungen beschreiben dabei, welche Funktionalitäten bedient werden (was die Software machen soll, z.B. einen PDF-Export erstellen). Nicht funktionale Anforderungen beschreiben, wie gut eine Funktion hinsichtlich Zuverlässigkeit, Benutzbarkeit und Performance erfüllt werden muss (z.B. wie schnell eine Funktion ausgeführt werden muss) \cite{sommerville_2011}.

Rahmenbedingungen, wie z.B. organisatorische Rahmenbedingungen, sind Anforderungen, welche die Realisierungsmöglichkeiten von Softwareprojekten einschränken und nur schwer oder gar nicht geändert werden können. Rahmenbedingungen können in vier Kategorien unterteilt werden:
\begin{description}
	\item [Technologisch]
	Bestehende IT-Infrastruktur, mit der die neue Software betrieben werden soll
	\item [Organisatorisch]
	Enthält vorgegebene Vorgehensmodelle und z.B. Strukturen der Organisation, welche die Software letzten Endes nutzt und welche sich z.B. in Form von Berechtigungskonzepten auf die Software auswirken
	\item [Rechtlich]
	Rechtliche Vorgaben, z.B. die EU-Datenschutz-Grundverordnung (DSGVO) im Datenschutz
	\item [Ethisch]
	Normen und Sitten des Kulturkreises der Anwender:innen, welche von der Software bewahrt werden müssen, z.B. Anredeformen
\end{description}
Formuliert werden diese Anforderungen von Personen und Organisationen mit Interesse an der geplanten Software (z.B. Nutzer:innen, Geldgeber:innen, Administrator:innen) sowie z.B. Regulierungsbehörden \cite{pohl_2010}.

In der Anforderungserhebung und -Analyse sollen die technischen Mitarbeiter:innen gemeinsam mit der Kundin oder dem Kunden die Anwendungsdomäne, die Dienste, bzw. die Funktionen herausfinden, die das System anbieten soll, sowie die betrieblichen Einschränkungen, denen das System unterliegt, klären. Ein großes Problem ist dabei, dass Stakeholder häufig selbst nicht genau wissen, was sie wollen oder brauchen. Hinzu kommt, dass sie Anforderungen in eigenen Worten, welche von den Fachtermen abweichen können, formulieren. Sind in einem Projekt verschiedene Menschen involviert, die Anforderungen definieren dürfen, so können diese sich auch in ihren Anforderungen widersprechen. Diesen Anforderungskonflikt aufzuklären gehört zu den Aufgaben des Requirements Engineer \cite{sommerville_2011}.

Existierende und damit bereits klar definierte Anforderungen lassen sich über Befragungen und Analysen ermitteln während innovative oder unklare Anforderungen am besten über Kreativitätstechniken ermittelt werden \cite{pohl_2010, rupp_2009}.

Zur Befragung können sowohl schriftliche als auch mündliche Befragungen vorgenommen werden. Dabei werden Anforderungen abgefragt, denen sich die eingebundenen Personen bewusst sind und die sie artikulieren können. Bei der Analyse wird mit abzulösender Bestandssoftware verglichen. Des Weiteren werden bestehende Dokumente wie z.B. Benutzerhandbücher untersucht. Zudem können auch Benutzer:innen beim Umgang mit Altsystemen beobachtet werden. Die Gefahr bei der Analyse ist u.A., dass die bestehenden Dokumente etc. bereits Fehler enthalten, die dann in das neue System übernommen werden \cite{rupp_2009}.

Da nur das grobe Ziel eine Plattform zu schaffen, auf der ein beendeter Workshop dokumentiert wird, um Beiträge und Anmerkungen von interessierten Stakeholdern einzusammeln, zu Beginn klar war, wurde hier Requirements Engineering mit Extreme Programming kombiniert. Es wurden iterativ Anforderungen definiert. Diese wurden spezifiziert und anschließend implementiert. Ein dabei entstandenes Artefakt wird \textit{Inkrement} genannt \cite{sommerville_2011}. Das Ergebnis wurde nachfolgend mit entsprechenden Stakeholdern besprochen. Auf Basis des in diesem Schritt entstandenen Inkrements wurden darauffolgend neue Anforderungen definiert oder im vorherigen Schritt definierte Anforderungen angepasst. Diese Schritte (Absprache von Anforderungen, Implementierung des Inkrements, Diskussion des Inkrements) wurden wiederholt bis das Artefakt einen Stand erreichte, welcher die unter \textit{\nameref{subsec:gegebenheiten_und_probleme_durch_den_aufbau_des_workshops}} definierten Anforderungen prototypisch erfüllte.

\subsection{Anforderungen}
\label{subsec:anforderungen}
Basierend auf der \nameref{sec:problemanalyse} (Kapitel \ref{sec:problemanalyse}) ergeben sich die im Folgenden dargelegten Anforderungen.

Da im späteren Produktivsystem mehr als eine Region gleichzeitig aktiv sein wird, benötigt die Plattform eine Möglichkeit, welche es zulässt, die Region zu der etwas beigetragen werden möchte, auszuwählen. Da die Liste der aktiven Regionen für den Prototypen sehr überschaubar sein wird, bietet sich eine einfache Liste an.

Um einen groben Überblick über das vorliegende Modell zu bekommen, sollte den Anwender:innen zuerst ein Video vorgespielt werden, welches den entsprechenden Überblick gewährt. 

Für die fachliche Qualifizierung der Benutzer:innen müssen eine Reihe Videos bereitgestellt werden, welche Informationen zur Verfügung stellen, welche zum Verständnis des gebauten Modells wichtig sind. Diese Videos sollten kurz sein und vollständig angesehen werden. Sie sollten außerdem auch bei einer Internetverbindung mit geringer Bandbreite zugreifbar sein.

Damit Anwender:innen sehen, was im Workshop erschaffen wurde, müssen ihnen die dokumentierten Ergebnisse des Workshops präsentiert werden. Dafür müssen sie die Möglichkeit bekommen, die erklärenden Videos zu sehen, welche die einzelnen Arbeitsergebnisse erklären. Auch für diese Videos gelten die Anforderungen der Videos aus der Qualifizierungsphase.

Abschließend wird zu jedem dokumentierten Video ein Feedback-Formular zur Verfügung gestellt. In diesem Formular können Teilnehmende einen Beitrag zur Verbesserung der Idee leisten, indem sie darlegen, was ihnen an der Idee gefällt, was sie sich zusätzlich wünschen würden und was sie konkret zur Idee ergänzen oder beitragen können.

Da unbekannt ist, welchen Kenntnisstand die Benutzer:innen haben (s. Kapitel \ref{fig:internet_knowledge_by_age_group}), wird es auf jeder Seite eine Audiohilfe geben um ggf. diejenigen abzuholen, die in einem konkreten Schritt Probleme haben.

Die Anforderungen können grob wie folgt zusammengefasst werden:
\begin{itemize}
	\item Funktionale Anforderungen
	\begin{itemize}
		\item Auswahl einer Region
		\item Ansehen des Überblickvideos
		\item Ansehen der Qualifizierungsvideos
		\item Ansehen der im Workshop dokumentierten Videos
		\item Beitrag zum dokumentierten Video abgeben
		\item Hilfestellung per Audio in jedem Prozessschritt
	\end{itemize}
	\item Nicht-Funktionale Anforderungen
	\begin{itemize}
		\item Einfache Bedienung
		\item Gute Videowiedergabe auch bei langsamer Internetanbindung
	\end{itemize}
\end{itemize}